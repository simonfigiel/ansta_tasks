from itertools import product
from decimal import Decimal

#ZADANIE 1
def _gen_range(start, stop):
    return (el for el in range(int(start), int(stop) + 1))

def process_zip_codes(zip_code_1='79-900', zip_code_2='80-155', sep='-'):
    """
    Return a list of all zip codes between the given params
    one with a specific range.
    :param zip_code_1: some zip code
    :type zip_code_1: string
    :param zip_code_2: as above
    :type zip_code_2: string
    :return list
    """
    obj = list(zip(zip_code_1.split(sep), zip_code_2.split(sep)))
    start1, stop1, start2, stop2 = sorted(obj[0]) + sorted(obj[1])

    # range_1 = (el for el in range(int(start1), int(stop1) + 1))
    # range_2 = (el for el in range(int(start2), int(stop2) + 1))
    range_1, range_2 = (_gen_range(start, stop)
                        for start, stop in [[start1, stop1], [start2, stop2]])

    zip_codes_list = (product(range_1, range_2))
    return [sep.join(map(str, el)) for el in zip_codes_list]

#ZADANIE 2
def diff_between_list_objects(obj, stop):
    """
    Return the difference between a given list object and newly generated
    one with a specific range.
    :param obj: a list of integers
    :type obj: list
    :param stop: the last element of the newly generated list object
    :type stop: integer
    :return list
    """
    return list(set([num for num in range(1, stop + 1)]).difference(set(obj)))

#ZADANIE 3
def gen_list_of_decimals(start, stop, step=.5):
    """
    Generate list of decimals
    :param start: the first element of the newly generated list object
    :type obj: integer
    :param stop: the last element of the list object
    :type stop: integer
    :param step: it specifies the increment
    :type step: integer only positive value
    :return generator
    """
    if stop == None:
        stop = start + 0.0
        start = 0.0
    while start < stop + step:
        yield Decimal(start)
        start += Decimal(step)

if __name__ == "__main__":
    print(f'ZADANIE 1: {process_zip_codes()}')
    print(f'ZADANIE 2: {diff_between_list_objects([2, 3, 7, 4, 9], 10)}')
    print(f'ZADANIE 3: {list(gen_list_of_decimals(2, 5.5))}')